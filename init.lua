--[[

N N         NB B B B B B
N  N        NB         B
N   N       NB         B
N    N      NB         B
N     N     NB B B B B B
N      N    NB         B
N       N   NB         B
N        N  NB         B
N         N NB B B B B B

@NSBuitrago Neovim config

--]]

require('core')
require('plugins')

-- The line beneath this is called `modeline`. See `:help modeline`
-- vim: ts=2 sts=2 sw=2 et
